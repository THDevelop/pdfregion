### PDF in Region darstllen
Hallo zusammen,   
hier möchte ich euch zeigen wie ihr simple eine PDF, das in der Datenbank vorhanden ist, in eine Region rendern könnt, ohne dabei auf Tools zurück zugreifen die Geld Kosten.
#### INFO
!!!! Das rendern eines PDF in einer Region funktioniert mit der Methode am besten im google Chrome, Firefox bietet einen link an !!!

Wie in meinen ganzen anderen Blogs werde ich auch bei diesem eine Step by Step Anleitung geben.

### Download
1. https://github.com/THDevelop/thdevelop_util_pkg 
2. https://github.com/pipwerks/PDFObject/blob/master/pdfobject.js
   
### 1. Runtergeladenen Dateien in die Applikation einbinden.
1. Das Package thdevelop_util_pkg kann über den SQL Workshop > SQL Commander eingespielt werden.
2. Die JS Datei pdfobject.js as Static Applicationfile einbinden Shared Components > Staticfilies

### 2. Erstellen einer seperaten Seite (Normal) für die Preview anzeige.
1. Das erstellen einer Seite sollte jedem bekannt sein. Diese Seite enthält eine Region mit einem Versteckten Item das die ID des PDF entgegennimmt.   
2. Erstellen eines Prozess Before Header.
   * hier benutzt ihr am besten das Package von mir, dies könnt ihr euch unter https://github.com/THDevelop/thdevelop_util_pkg runterladen.
   * In dem Package ist eine Procedure get_preview, diese wird benötigt um das PDF (blob File) aus der Datenbak an die Anwendung zu geben.

  ```python
   get_preview(pi_pdf_id_column in varchar2,
               pi_filename_column in varchar2,
               pi_bolb_column in varchar2,
               pi_tablename in varchar2,
               pi_pdf_id in number
             );
  ```
  Die Parameter sollten selbsterklärend sein.   


### 3. Wechsel zur seite wo das PDF in einer Region angezeigt werden soll.
1. Erstellen einer neuen Region vom 
   + Type: Static Content
   + Bei Advanced eine Static ID hinzufügen.
   + Die größe der Region definiert ihr im gleichen bereicht unter Custom Attributes z.B STYLE="height:1000px"
   * Zusammenfassung   
     Type: Static Content   
     Static ID: z.B. pdf_region   
     Custom Attributes: STYLE="height:1000px"

2. Die Magie einbinden
Hier gibt es 2 Wege, der Unterschied dazwischen ist, dass einmal das PDF per Select List angezeigt    
werden soll und der andere über eine Report Spalte. 
   * Anzeige per Select List
     + Hinzufügen eines versteckten Items das eine URL entgegenimmt. z.B. P2_PREVIEW_URL.
     + Auf der Select List ,diese muss als return value die ID des PDF in der Datenbank haben, erstellt    
       ihr eine Dynamic Action (on Change)
        1. TURE
           * Action: SetValue    
             Set Type: PL/SQL Function Body   
             PL/SQL Function Body   
             ```python
             return APEX_PAGE.GET_URL (p_page   => 3, -- Page step 2
                                       p_items  => 'P3_REPORTID', -- Hidden item step 2.1
                                       p_values => :P2_BLOB );  -- select list where you make your dynamic action
             ```
             Affected Elements   
             Selection Type: Item(s)   
             Item(s): Your hidden item to hold the URL e.q. P2_PREVIEW_URL

        2. TRUE
           * Action: Execute JavaScriptCode   
             Code
            ```JS
            var src = $v("P2_PREVIEW_URL"); // get the value of the item (the pre URL)
            PDFObject.embed(src, "#pdf_region"); // Render the pdf in the region
            ```
   * Anzeige per Report Spalte
      + Erstellen einer Dynamci Action
        * Name: ShowPDF   
          When 
            * Event: Custom   
              CustomEvent: ShowPDF   
              Selection Type: JavaScript Expression   
              JavaScript Expression: document
          * TURE Action
            Action: Execute JavaScript Code
            Code: PDFObject.embed(this.data.url, "#pdf_region"); 
      + Wenn noch keine Report vorhanden ist müsst ihr ihn erstellen
        + Das SQL Statement des Report, sollte ungefähr wie im beispiel code aussehen.
          ```sql
          select PDF_REGION.FILENAME as FILENAME ,
                PDF_REGION.PK_ID as PK_ID    ,
                APEX_PAGE.GET_URL ( p_page   => 3,
                                    p_items  => 'P3_REPORTID',
                                    p_values => PDF_REGION.PK_ID ) as URL
          from PDF_REGION PDF_REGION
          ```
          In der Abfrage wird der benötigte Link schon mit erzeugt.

      + Nach dem der Report erstellt wurde, sucht euch eine Spalte aus die ihr verwenden wollt um   
        mit einem Klick das PDF in der dafür angelegten Region anzeigen zu lassen.
        * Type: Link   
          Link 
            *  Target Type: URL    
                URL : javascript:apex.event.trigger(document, 'ShowPDF', [{url:'#URL#'}]);void(0);


### Erklärung Javascript Code in URL
```JS
javascript:apex.event.trigger(document, 'ShowPDF', [{url:'#URL#'}]);void(0);
```
Zur Erklärung werde ich die Funktion so umschreiben das das die benötigten Parameter die Erklärung erläutern sollen.

```JS
javascript:apex.event.trigger(document, 'HereYourDynamicActionFunction', [{url:'#HereYourColumnThatContainsTheLink#'}]);void(0);
```

### So einfach kann man eine PDF in einer Region anzeigen lassen.
### Danke und viel Spaß beim nachmachen.

### Demo Link 
https://apex.oracle.com/pls/apex/f?p=9414 