```python
procedure get_preview(pi_pdf_id_column in varchar2,
                        pi_filename_column in varchar2,
                        pi_bolb_column in varchar2,
                        pi_tablename in varchar2,
                        pi_pdf_id in number,
                        pi_sql_statement in varchar2 default null
                      )
  as
    l_file blob;
    l_filename varchar2(4000);
    l_sql varchar2(4000);
  begin

    l_sql := 'select ' || dbms_assert.simple_sql_name(pi_filename_column) ||
             '     , ' || dbms_assert.simple_sql_name(pi_bolb_column) ||
             '  from ' || dbms_assert.sql_object_name(pi_tablename) ||
             ' where ' || dbms_assert.simple_sql_name(pi_pdf_id_column) || ' = :pdf_id'
    ;
   EXECUTE IMMEDIATE l_sql
                 INTO l_filename,
                      l_file
                USING pi_pdf_id;  


  sys.htp.init;
  sys.owa_util.mime_header( 'application/pdf', false );
  sys.htp.p('Content-length: ' || sys.dbms_lob.getlength( l_file));
  sys.htp.p('Content-Disposition: inline; filename="' || l_filename ||
  '"' );
  sys.owa_util.http_header_close;
  sys.wpg_docload.download_file( l_file );

  apex_application.stop_apex_engine;
  exception
  when others then
  raise;
  end get_preview;

```